namespace MusicLibrarySuite.MicroserviceClients;

/// <summary>
/// Represents an empty marker interface for microservice clients.
/// </summary>
public interface IMicroserviceClient
{
}
