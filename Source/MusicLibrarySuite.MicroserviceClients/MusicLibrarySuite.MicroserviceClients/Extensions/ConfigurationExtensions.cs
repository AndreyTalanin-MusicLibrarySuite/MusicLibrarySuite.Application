using Microsoft.Extensions.Configuration;

namespace MusicLibrarySuite.MicroserviceClients.Extensions;

/// <summary>
/// Provides a set of extension methods for the <see cref="IConfiguration" /> interface.
/// </summary>
public static class ConfigurationExtensions
{
    /// <summary>
    /// Represents a shorthand method for the <c>GetSection("MicroserviceUrls")[microserviceName]</c> expression.
    /// </summary>
    /// <param name="configuration">The application configuration.</param>
    /// <param name="microserviceName">The microservice name.</param>
    /// <returns>The microservice URL string or <see langword="null" />.</returns>
    public static string? GetMicroserviceUrl(this IConfiguration configuration, string microserviceName)
    {
        return configuration?.GetSection("MicroserviceUrls")?[microserviceName];
    }
}
