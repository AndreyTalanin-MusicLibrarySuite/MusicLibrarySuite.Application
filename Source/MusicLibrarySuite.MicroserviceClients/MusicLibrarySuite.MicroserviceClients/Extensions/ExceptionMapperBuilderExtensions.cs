using System;
using System.Diagnostics;

using Microsoft.AspNetCore.Mvc;

using MusicLibrarySuite.AspNetCore.Diagnostics;
using MusicLibrarySuite.Infrastructure.Exceptions.Delegates;
using MusicLibrarySuite.Infrastructure.Exceptions.Services.Abstractions;
using MusicLibrarySuite.MicroserviceClients.Exceptions;

namespace MusicLibrarySuite.MicroserviceClients.Extensions;

/// <summary>
/// Provides a set of extension methods for the <see cref="IExceptionMapperBuilder" /> interface.
/// </summary>
public static class ExceptionMapperBuilderExtensions
{
    /// <summary>
    /// Adds a new exception mapping rule for <see cref="ApiException" /> exceptions.
    /// </summary>
    /// <param name="exceptionMapperBuilder">The <see cref="IExceptionMapperBuilder" /> instance.</param>
    /// <param name="predicate">The predicate checking whether a thrown <see cref="ApiException" /> exception should be mapped.</param>
    /// <param name="factoryMethod">The factory method creating a new mapped exception, providing the original <see cref="ApiException" /> exception as context.</param>
    public static void AddApiExceptionRule(
        this IExceptionMapperBuilder exceptionMapperBuilder,
        ExceptionPredicate<ApiException> predicate,
        ExceptionFactoryMethod<ApiException> factoryMethod)
    {
        exceptionMapperBuilder.AddRule(predicate, factoryMethod);
    }

    /// <summary>
    /// Adds a new exception mapping rule for <see cref="ApiException" /> exceptions.
    /// </summary>
    /// <param name="exceptionMapperBuilder">The <see cref="IExceptionMapperBuilder" /> instance.</param>
    /// <param name="statusCode">The HTTP status code to initially filter <see cref="ApiException" /> exceptions.</param>
    /// <param name="predicate">The predicate checking whether a thrown <see cref="ApiException" /> exception should be mapped.</param>
    /// <param name="factoryMethod">The factory method creating a new mapped exception, providing the original <see cref="ApiException" /> exception as context.</param>
    public static void AddApiExceptionRule(
        this IExceptionMapperBuilder exceptionMapperBuilder,
        int statusCode,
        ExceptionPredicate<ApiException> predicate,
        ExceptionFactoryMethod<ApiException> factoryMethod)
    {
        bool ApiExceptionPredicate(ApiException apiException) => apiException.StatusCode == statusCode && predicate(apiException);

        exceptionMapperBuilder.AddApiExceptionRule(ApiExceptionPredicate, factoryMethod);
    }

    /// <summary>
    /// Adds a new exception mapping rule for <see cref="ApiException{ProblemDetails}" /> exceptions.
    /// </summary>
    /// <param name="exceptionMapperBuilder">The <see cref="IExceptionMapperBuilder" /> instance.</param>
    /// <param name="predicate">The predicate checking whether a thrown <see cref="ApiException{ProblemDetails}" /> exception should be mapped.</param>
    /// <param name="factoryMethod">The factory method creating a new mapped exception, providing the original <see cref="ApiException{ProblemDetails}" /> exception as context.</param>
    public static void AddApiExceptionRule(
        this IExceptionMapperBuilder exceptionMapperBuilder,
        ExceptionPredicate<ApiException<ProblemDetails>> predicate,
        ExceptionFactoryMethod<ApiException<ProblemDetails>> factoryMethod)
    {
        exceptionMapperBuilder.AddRule(predicate, factoryMethod);
    }

    /// <summary>
    /// Adds a new exception mapping rule for <see cref="ApiException{ProblemDetails}" /> exceptions.
    /// </summary>
    /// <param name="exceptionMapperBuilder">The <see cref="IExceptionMapperBuilder" /> instance.</param>
    /// <param name="statusCode">The HTTP status code to initially filter <see cref="ApiException" /> exceptions.</param>
    /// <param name="predicate">The predicate checking whether a thrown <see cref="ApiException{ProblemDetails}" /> exception should be mapped.</param>
    /// <param name="factoryMethod">The factory method creating a new mapped exception, providing the original <see cref="ApiException{ProblemDetails}" /> exception as context.</param>
    public static void AddApiExceptionRule(
        this IExceptionMapperBuilder exceptionMapperBuilder,
        int statusCode,
        ExceptionPredicate<ApiException<ProblemDetails>> predicate,
        ExceptionFactoryMethod<ApiException<ProblemDetails>> factoryMethod)
    {
        bool ApiExceptionPredicate(ApiException<ProblemDetails> apiException) => apiException.StatusCode == statusCode && predicate(apiException);

        exceptionMapperBuilder.AddRule(ApiExceptionPredicate, factoryMethod);
    }

    /// <summary>
    /// Adds a new exception mapping rule for <see cref="ApiException{ProblemDetails}" /> exceptions.
    /// </summary>
    /// <param name="exceptionMapperBuilder">The <see cref="IExceptionMapperBuilder" /> instance.</param>
    /// <param name="statusCode">The HTTP status code to initially filter <see cref="ApiException" /> exceptions.</param>
    /// <param name="remoteExceptionType">The remote exception type to initially filter <see cref="ApiException" /> exceptions.</param>
    /// <param name="predicate">The predicate checking whether a thrown <see cref="ApiException{ProblemDetails}" /> exception should be mapped.</param>
    /// <param name="factoryMethod">The factory method creating a new mapped exception, providing the original <see cref="ApiException{ProblemDetails}" /> exception as context.</param>
    public static void AddApiExceptionRule(
        this IExceptionMapperBuilder exceptionMapperBuilder,
        int statusCode,
        Type remoteExceptionType,
        ExceptionPredicate<ApiException<ProblemDetails>> predicate,
        ExceptionFactoryMethod<ApiException<ProblemDetails>> factoryMethod)
    {
        bool ApiExceptionPredicate(ApiException<ProblemDetails> apiException)
        {
            bool result = true;

            result &= apiException.StatusCode == statusCode;

            ProblemDetails problemDetails = apiException.Result;
            string expectedExceptionTypeFullName = remoteExceptionType.FullName
                ?? throw new UnreachableException($"The {nameof(remoteExceptionType)} type has no full name");
            string? actualExceptionTypeFullName = problemDetails.Extensions.TryGetValue(ProblemDetailsExtensionConstants.ExceptionTypeKey, out object? exceptionType)
                ? (string?)exceptionType
                : null;

            result &= expectedExceptionTypeFullName.Equals(actualExceptionTypeFullName, StringComparison.InvariantCultureIgnoreCase);
            result &= predicate(apiException);

            return result;
        }

        exceptionMapperBuilder.AddRule(ApiExceptionPredicate, factoryMethod);
    }
}
