using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Net.Http;
using System.Reflection;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Http;

namespace MusicLibrarySuite.MicroserviceClients.Extensions;

/// <summary>
/// Provides a set of extension methods for the <see cref="IServiceCollection" /> interface.
/// </summary>
public static class ServiceCollectionExtensions
{
    /// <summary>
    /// Configures a binding between the <typeparamref name="TClientService" /> type and a named <see cref="HttpClient" />.
    /// The client name will be set to the type name of <typeparamref name="TClientService" />.
    /// </summary>
    /// <remarks>This method is accessed dynamically. See <see cref="AddMicroserviceClient{TClientService}(IServiceCollection)" /> for details.</remarks>
    /// <typeparam name="TClientService">
    /// The microservice client type.
    /// The type specified will be registered in the service collection as a transient service.
    /// See <see cref="ITypedHttpClientFactory{TClientService}" /> for more details about authoring typed clients.
    /// </typeparam>
    /// <typeparam name="TClientImplementation">
    /// The implementation client type.
    /// The type specified will be instantiated by the <see cref="ITypedHttpClientFactory{TImplementation}" /> instance.
    /// </typeparam>
    /// <param name="services">The <see cref="IServiceCollection" /> collection to add services to.</param>
    /// <returns>The same service collection so that multiple calls can be chained.</returns>
    /// <exception cref="InvalidOperationException">Thrown if a microservice URL is not found in the configuration.</exception>
    public static IServiceCollection AddMicroserviceClient<TClientService, [DynamicallyAccessedMembers(DynamicallyAccessedMemberTypes.PublicConstructors)] TClientImplementation>(this IServiceCollection services)
        where TClientService : class, IMicroserviceClient
        where TClientImplementation : class, TClientService, IMicroserviceClient
    {
        string clientTypeName = typeof(TClientImplementation).Name;
        services.AddHttpClient<TClientService, TClientImplementation>((serviceProvider, httpClient) =>
        {
            IConfiguration configuration = serviceProvider.GetRequiredService<IConfiguration>();

            string? microserviceUrl = configuration.GetMicroserviceUrl(clientTypeName[..clientTypeName.LastIndexOf("Client")]);
            if (string.IsNullOrEmpty(microserviceUrl))
            {
                throw new InvalidOperationException($"Unable to find a microservice URL for the '{clientTypeName}' client.");
            }

            httpClient.BaseAddress = new Uri(microserviceUrl.TrimEnd('/') + "/");
        });

        return services;
    }

    /// <summary>
    /// Configures a binding between the <typeparamref name="TClientService" /> type and a named <see cref="HttpClient" />.
    /// The client name will be set to the type name of <typeparamref name="TClientService" />.
    /// <para>The implementation type will be looked up in the same assembly the <typeparamref name="TClientService" /> client service type is located in.</para>
    /// </summary>
    /// <typeparam name="TClientService">
    /// The microservice client type.
    /// The type specified will be registered in the service collection as a transient service.
    /// See <see cref="ITypedHttpClientFactory{TClientService}" /> for more details about authoring typed clients.
    /// </typeparam>
    /// <param name="services">The <see cref="IServiceCollection" /> collection to add services to.</param>
    /// <returns>The same service collection so that multiple calls can be chained.</returns>
    /// <exception cref="InvalidOperationException">Thrown if an implementation type for the <typeparamref name="TClientService" /> client service type is not found.</exception>
    /// <exception cref="InvalidOperationException">Thrown if a microservice URL is not found in the configuration.</exception>
    [SuppressMessage("Style", "IDE0270:Use coalesce expression", Justification = "To increase readability of the execution flow after the multi-line LINQ query.")]
    public static IServiceCollection AddMicroserviceClient<TClientService>(this IServiceCollection services)
        where TClientService : class, IMicroserviceClient
    {
        Type microserviceClientServiceType = typeof(TClientService);
        Assembly microserviceClientServiceTypeAssembly = microserviceClientServiceType.Assembly;

        Type? microserviceClientImplementationType = microserviceClientServiceTypeAssembly.GetTypes()
            .Where(type => type.IsAssignableTo(microserviceClientServiceType) && !type.IsInterface)
            .SingleOrDefault();

        if (microserviceClientImplementationType is null)
        {
            string clientServiceTypeName = typeof(TClientService).Name;
            throw new InvalidOperationException($"Unable to find an implementation type for the '{clientServiceTypeName}' client service type.");
        }

        MethodInfo? methodInfo = typeof(ServiceCollectionExtensions).GetMethods()
            .Where(methodInfo => methodInfo.Name == nameof(AddMicroserviceClient) && methodInfo.IsGenericMethodDefinition)
            .Where(methodInfo => methodInfo.GetGenericArguments().Length == 2 && methodInfo.GetGenericArguments()[0].Name == nameof(TClientService))
            .SingleOrDefault();

        if (methodInfo is null)
        {
            throw new UnreachableException($"The {nameof(AddMicroserviceClient)} method overload that accepts an explicit implementation type was not found.");
        }

        MethodInfo typedMethodInfo = methodInfo.MakeGenericMethod(microserviceClientServiceType, microserviceClientImplementationType);
        typedMethodInfo.Invoke(null, [services]);

        return services;
    }
}
