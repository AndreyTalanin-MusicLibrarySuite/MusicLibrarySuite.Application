using System;
using System.Collections.Generic;

using Microsoft.AspNetCore.WebUtilities;

namespace MusicLibrarySuite.MicroserviceClients.Exceptions;

/// <summary>
/// Represents an exception for an internal API method.
/// </summary>
/// <remarks>See the <see href="https://github.com/RicoSuter/NSwag/issues/2839" /> issue for details.</remarks>
public class ApiException : Exception
{
    /// <summary>
    /// Gets the HTTP status code.
    /// </summary>
    public int StatusCode { get; private set; }

    /// <summary>
    /// Gets the HTTP status message.
    /// </summary>
    public string? StatusMessage { get; private set; }

    /// <summary>
    /// Gets the HTTP response.
    /// </summary>
    public string? Response { get; private set; }

    /// <summary>
    /// Gets the HTTP headers.
    /// </summary>
    public IReadOnlyDictionary<string, IEnumerable<string>> Headers { get; private set; }

    /// <summary>
    /// Initializes a new instance of the <see cref="ApiException" /> type using the specified values.
    /// </summary>
    /// <param name="message">The error message that describes the current exception.</param>
    /// <param name="statusCode">The HTTP status code.</param>
    /// <param name="response">The HTTP response.</param>
    /// <param name="headers">The HTTP headers.</param>
    /// <param name="innerException">The <see cref="Exception" /> instance that caused the current exception.</param>
    public ApiException(string message, int statusCode, string? response, IReadOnlyDictionary<string, IEnumerable<string>> headers, Exception? innerException)
        : base(FormatMessage(message, statusCode, response), innerException)
    {
        StatusCode = statusCode;
        StatusMessage = !string.IsNullOrEmpty(StatusMessage = ReasonPhrases.GetReasonPhrase(statusCode))
            ? StatusMessage
            : null;
        Response = response;
        Headers = headers;
    }

    /// <inheritdoc />
    public override string ToString()
    {
        return $"HTTP Response: \n\n{Response}\n\n{base.ToString()}";
    }

    private static string FormatMessage(string message, int statusCode, string? response)
    {
        string statusCodeString = $"HTTP Status Code: {statusCode}";
        string statusMessageSuffixString = !string.IsNullOrEmpty(statusMessageSuffixString = ReasonPhrases.GetReasonPhrase(statusCode))
            ? $" ({statusMessageSuffixString})"
            : string.Empty;
        string responseTitleString = $"HTTP Response (First 512 Bytes):";
        string responseContentString = response == null ? "(null)" : response[..(response.Length >= 512 ? 512 : response.Length)];
        return $"{message}\n\n{statusCodeString}{statusMessageSuffixString}\n{responseTitleString}\n{responseContentString}";
    }
}

/// <summary>
/// Represents an exception for an internal API method that returns a value.
/// </summary>
/// <typeparam name="TResult">The type of the result returned by the method.</typeparam>
/// <remarks>See the <see href="https://github.com/RicoSuter/NSwag/issues/2839" /> issue for details.</remarks>
public class ApiException<TResult> : ApiException
{
    /// <summary>
    /// Gets the result of the API call.
    /// </summary>
    public TResult Result { get; private set; }

    /// <summary>
    /// Initializes a new instance of the <see cref="ApiException{TResult}" /> type using the specified values.
    /// </summary>
    /// <param name="message">The error message that describes the current exception.</param>
    /// <param name="statusCode">The HTTP status code.</param>
    /// <param name="response">The HTTP response.</param>
    /// <param name="headers">The HTTP headers.</param>
    /// <param name="result">The result of the API call.</param>
    /// <param name="innerException">The <see cref="Exception" /> instance that caused the current exception.</param>
    public ApiException(string message, int statusCode, string? response, IReadOnlyDictionary<string, IEnumerable<string>> headers, TResult result, Exception? innerException)
        : base(message, statusCode, response, headers, innerException)
    {
        Result = result;
    }
}
