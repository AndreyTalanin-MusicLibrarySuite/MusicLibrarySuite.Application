namespace MusicLibrarySuite.MicroserviceClients.CatalogService;

/// <summary>
/// Describes a <c>MusicLibrarySuite.CatalogService</c> microservice client.
/// </summary>
public partial interface ICatalogServiceClient : IMicroserviceClient
{
}
