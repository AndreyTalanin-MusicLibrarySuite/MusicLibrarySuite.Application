using Microsoft.Extensions.DependencyInjection;

using MusicLibrarySuite.Application.Middlewares;
using MusicLibrarySuite.CatalogService.Contracts.Services;
using MusicLibrarySuite.CatalogService.Contracts.Services.Abstractions;

// Disable the IDE0079 (Remove unnecessary suppression) notification due to false positive alerts.
#pragma warning disable IDE0079

namespace MusicLibrarySuite.Application.Extensions;

/// <summary>
/// Provides a set of extension methods for the <see cref="IServiceCollection" /> interface.
/// </summary>
public static class ServiceCollectionExtensions
{
    /// <summary>
    /// Registers the CMS (Content Management System) services in the specified <see cref="IServiceCollection" /> collection.
    /// </summary>
    /// <param name="services">The <see cref="IServiceCollection" /> collection for adding service descriptors.</param>
    /// <returns>The same service collection so that multiple calls can be chained.</returns>
    public static IServiceCollection AddCmsServices(this IServiceCollection services)
    {
#pragma warning disable IDE0001
        services.AddTransient<ICatalogService, MusicLibrarySuite.Application.Core.Services.CatalogProxyService>();
        services.AddTransient<ICatalogRelationshipServiceExtension, MusicLibrarySuite.Application.Core.Services.CatalogRelationshipProxyServiceExtension>();
        services.AddTransient<ICatalogServiceFacade, CatalogServiceFacade>();

        services.AddTransient<ICatalogNodeService, MusicLibrarySuite.Application.Core.Services.CatalogNodeProxyService>();
        services.AddTransient<ICatalogNodeToCatalogRelationshipServiceExtension, MusicLibrarySuite.Application.Core.Services.CatalogNodeToCatalogRelationshipProxyServiceExtension>();
        services.AddTransient<ICatalogNodeHierarchicalRelationshipServiceExtension, MusicLibrarySuite.Application.Core.Services.CatalogNodeHierarchicalRelationshipProxyServiceExtension>();
        services.AddTransient<ICatalogNodeRelationshipServiceExtension, MusicLibrarySuite.Application.Core.Services.CatalogNodeRelationshipProxyServiceExtension>();
        services.AddTransient<ICatalogNodeServiceFacade, CatalogNodeServiceFacade>();

        services.AddTransient<ICatalogEntryService, MusicLibrarySuite.Application.Core.Services.CatalogEntryProxyService>();
        services.AddTransient<ICatalogEntryToCatalogRelationshipServiceExtension, MusicLibrarySuite.Application.Core.Services.CatalogEntryToCatalogRelationshipProxyServiceExtension>();
        services.AddTransient<ICatalogEntryToCatalogNodeRelationshipServiceExtension, MusicLibrarySuite.Application.Core.Services.CatalogEntryToCatalogNodeRelationshipProxyServiceExtension>();
        services.AddTransient<ICatalogEntryHierarchicalRelationshipServiceExtension, MusicLibrarySuite.Application.Core.Services.CatalogEntryHierarchicalRelationshipProxyServiceExtension>();
        services.AddTransient<ICatalogEntryHierarchicalRelationshipTypeServiceExtension, MusicLibrarySuite.Application.Core.Services.CatalogEntryHierarchicalRelationshipTypeProxyServiceExtension>();
        services.AddTransient<ICatalogEntryRelationshipServiceExtension, MusicLibrarySuite.Application.Core.Services.CatalogEntryRelationshipProxyServiceExtension>();
        services.AddTransient<ICatalogEntryRelationshipTypeServiceExtension, MusicLibrarySuite.Application.Core.Services.CatalogEntryRelationshipTypeProxyServiceExtension>();
        services.AddTransient<ICatalogEntryTypeServiceExtension, MusicLibrarySuite.Application.Core.Services.CatalogEntryTypeProxyServiceExtension>();
        services.AddTransient<ICatalogEntryServiceFacade, CatalogEntryServiceFacade>();
#pragma warning restore IDE0001

        return services;
    }

    /// <summary>
    /// Registers the <see cref="ForwardStatusCodeMiddleware" /> middleware in the specified <see cref="IServiceCollection" /> collection.
    /// </summary>
    /// <param name="services">The <see cref="IServiceCollection" /> collection for adding service descriptors.</param>
    /// <returns>The same service collection so that multiple calls can be chained.</returns>
    public static IServiceCollection AddStatusCodeForwarding(this IServiceCollection services)
    {
        services.AddScoped<ForwardStatusCodeMiddleware>();

        return services;
    }
}
