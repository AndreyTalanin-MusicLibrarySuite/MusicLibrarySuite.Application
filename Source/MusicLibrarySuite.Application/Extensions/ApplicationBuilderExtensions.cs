using System;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

using MusicLibrarySuite.Application.Configuration;
using MusicLibrarySuite.Application.Middlewares;
using MusicLibrarySuite.AspNetCore.SpaServices.Extensions;

namespace MusicLibrarySuite.Application.Extensions;

/// <summary>
/// Provides a set of extension methods for the <see cref="IApplicationBuilder" /> interface.
/// </summary>
public static class ApplicationBuilderExtensions
{
    /// <summary>
    /// Configures the application to use the React development server (provided by Vite) for the Single Page Application (SPA).
    /// </summary>
    /// <param name="applicationBuilder">The request pipeline builder.</param>
    /// <param name="configure">An <see cref="Action{T}"/> method to configure the provided <see cref="SpaDevelopmentServerOptions"/> options.</param>
    public static void UseSpa(this IApplicationBuilder applicationBuilder, Action<SpaDevelopmentServerOptions> configure)
    {
        IServiceProvider serviceProvider = applicationBuilder.ApplicationServices;

        IConfiguration configuration = serviceProvider.GetRequiredService<IConfiguration>();
        IWebHostEnvironment webHostEnvironment = serviceProvider.GetRequiredService<IWebHostEnvironment>();

        SpaDevelopmentServerOptions spaDevelopmentServerOptions = new();

        configure(spaDevelopmentServerOptions);

        applicationBuilder.UseSpa(spaBuilder =>
        {
            spaBuilder.Options.SourcePath = spaDevelopmentServerOptions.SourcePath;
            spaBuilder.Options.PackageManagerCommand = spaDevelopmentServerOptions.PackageManagerCommand;

            if (webHostEnvironment.IsDevelopment())
            {
                SpaDevelopmentServerConfiguration spaDevelopmentServerConfiguration = new();
                configuration.GetSection(SpaDevelopmentServerConfiguration.SectionName)
                    .Bind(spaDevelopmentServerConfiguration);

                spaBuilder.UseEmptyAcceptEncodingHeader();

                if (spaDevelopmentServerConfiguration.UseExternalDevelopmentServer)
                {
                    string developmentServerBaseUri = spaDevelopmentServerConfiguration.ExternalDevelopmentServerBaseUri
                        ?? throw new InvalidOperationException("Unable to read the Single Page Application (SPA) development server base URI from the configuration.");

                    spaBuilder.UseProxyToSpaDevelopmentServer(developmentServerBaseUri);
                }
                else
                {
                    spaBuilder.UseViteDevelopmentServer(spaDevelopmentServerOptions.Script);
                }
            }
        });
    }

    /// <summary>
    /// Configures the application to use the <see cref="ForwardStatusCodeMiddleware" /> middleware.
    /// </summary>
    /// <param name="applicationBuilder">The request pipeline builder.</param>
    public static void UseStatusCodeForwarding(this IApplicationBuilder applicationBuilder)
    {
        applicationBuilder.UseMiddleware<ForwardStatusCodeMiddleware>();
    }
}
