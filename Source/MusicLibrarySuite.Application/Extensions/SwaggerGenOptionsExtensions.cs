using System;
using System.IO;

using Microsoft.Extensions.DependencyInjection;

using Swashbuckle.AspNetCore.SwaggerGen;

namespace MusicLibrarySuite.Application.Extensions;

/// <summary>
/// Provides a set of extension methods for the <see cref="SwaggerGenOptions" /> interface.
/// </summary>
public static class SwaggerGenOptionsExtensions
{
    private const string c_aspNetCoreXmlDocsFileName = "MusicLibrarySuite.AspNetCore.Mvc.xml";
    private const string c_applicationXmlDocsFileName = "MusicLibrarySuite.Application.xml";
    private const string c_applicationContractsXmlDocsFileName = "MusicLibrarySuite.Application.Contracts.xml";

    /// <summary>
    /// Injects human-friendly descriptions for Operations, Parameters and Schemas based on XML Comment files for built-in ASP.NET Core types.
    /// </summary>
    /// <param name="swaggerGenOptions">The <see cref="SwaggerGenOptions" /> instance.</param>
    public static void IncludeAspNetCoreXmlComments(this SwaggerGenOptions swaggerGenOptions)
    {
        swaggerGenOptions.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, c_aspNetCoreXmlDocsFileName));
    }

    /// <summary>
    /// Injects human-friendly descriptions for Operations, Parameters and Schemas based on XML Comment files for application API controllers.
    /// </summary>
    /// <param name="swaggerGenOptions">The <see cref="SwaggerGenOptions" /> instance.</param>
    public static void IncludeApplicationXmlComments(this SwaggerGenOptions swaggerGenOptions)
    {
        string applicationXmlDocsFileName = Path.Combine(AppContext.BaseDirectory, c_applicationXmlDocsFileName);
        swaggerGenOptions.IncludeXmlComments(applicationXmlDocsFileName, includeControllerXmlComments: true);
    }

    /// <summary>
    /// Injects human-friendly descriptions for Operations, Parameters and Schemas based on XML Comment files for application API models and service interfaces.
    /// </summary>
    /// <param name="swaggerGenOptions">The <see cref="SwaggerGenOptions" /> instance.</param>
    public static void IncludeApplicationContractsXmlComments(this SwaggerGenOptions swaggerGenOptions)
    {
        string contractsXmlDocsFileName = Path.Combine(AppContext.BaseDirectory, c_applicationContractsXmlDocsFileName);
        swaggerGenOptions.IncludeXmlComments(contractsXmlDocsFileName);
    }
}
