using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Text.Json;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using MusicLibrarySuite.AspNetCore.Diagnostics;
using MusicLibrarySuite.Infrastructure.Exceptions;
using MusicLibrarySuite.MicroserviceClients.Exceptions;

namespace MusicLibrarySuite.Application.Middlewares;

/// <summary>
/// Represents a middleware that produces an HTTP response represented by the <see cref="MusicLibrarySuiteForwardStatusCodeException" /> exception.
/// </summary>
/// <remarks>
/// The <see cref="MusicLibrarySuiteForwardStatusCodeException" /> exception must contain an <see cref="ApiException" /> exception in its <see cref="Exception.InnerException" /> property
/// in order for this middleware to be able to process the exception.
/// </remarks>
public class ForwardStatusCodeMiddleware : IMiddleware
{
    private const string c_invalidTraceId = "invalid-traceId-value";

    private static readonly JsonSerializerOptions s_webJsonSerializerOptions = new(JsonSerializerDefaults.Web);

    private readonly IProblemDetailsService m_problemDetailsService;
    private readonly ILogger<ForwardStatusCodeMiddleware> m_logger;

    /// <summary>
    /// Initializes a new instance of the <see cref="ForwardStatusCodeMiddleware" /> type using the specified services.
    /// </summary>
    /// <param name="problemDetailsService">The <see cref="IProblemDetailsService" /> instance to create a <see cref="ProblemDetails" /> response.</param>
    /// <param name="logger">The <see cref="ILogger{ForwardStatusCodeMiddleware}" /> logger instance.</param>
    public ForwardStatusCodeMiddleware(IProblemDetailsService problemDetailsService, ILogger<ForwardStatusCodeMiddleware> logger)
    {
        m_problemDetailsService = problemDetailsService;
        m_logger = logger;
    }

    /// <inheritdoc />
    public async Task InvokeAsync(HttpContext httpContext, RequestDelegate next)
    {
        try
        {
            await next.Invoke(httpContext);
        }
        catch (MusicLibrarySuiteForwardStatusCodeException exception)
        {
            ProblemDetails? problemDetails;
            if (exception.InnerException is ApiException<ProblemDetails> apiExceptionDetailed)
                problemDetails = apiExceptionDetailed.Result;
            else if (exception.InnerException is not ApiException apiException || !TryDeserializeProblemDetails(apiException.Response, out problemDetails))
                throw;

            int statusCode = exception.StatusCode;
            string originalTraceId = GetOriginalTraceId(problemDetails);
            string applicationTraceId = GetApplicationTraceId(httpContext);

            WriteOriginalTraceIdToLogs(statusCode, originalTraceId);
            SetApplicationTraceIdToProblemDetails(problemDetails, applicationTraceId);

            httpContext.Response.StatusCode = statusCode;

            await m_problemDetailsService.TryWriteAsync(new ProblemDetailsContext
            {
                HttpContext = httpContext,
                ProblemDetails = problemDetails,
                Exception = exception,
            });
        }
    }

    private static bool TryDeserializeProblemDetails(string? response, [NotNullWhen(true)] out ProblemDetails? problemDetails)
    {
        problemDetails = null;
        if (string.IsNullOrEmpty(response))
            return false;

        try
        {
            problemDetails = JsonSerializer.Deserialize<ProblemDetails>(response, s_webJsonSerializerOptions);
            return problemDetails is not null;
        }
        catch (JsonException)
        {
            return false;
        }
    }

    private static string GetApplicationTraceId(HttpContext httpContext)
    {
        string applicationTraceId = Activity.Current?.Id ?? httpContext.TraceIdentifier;
        return applicationTraceId;
    }

    private static string GetOriginalTraceId(ProblemDetails problemDetails)
    {
        if (!problemDetails.Extensions.TryGetValue(ProblemDetailsExtensionConstants.TraceIdKey, out object? originalTraceIdObject))
            return c_invalidTraceId;

        string? originalTraceId = originalTraceIdObject is not JsonElement originalTraceIdJsonElement
            || string.IsNullOrEmpty(originalTraceId = originalTraceIdJsonElement.GetString())
            ? c_invalidTraceId
            : originalTraceId;

        return originalTraceId;
    }

    private static void SetApplicationTraceIdToProblemDetails(ProblemDetails problemDetails, string applicationTraceId)
    {
        problemDetails.Extensions[ProblemDetailsExtensionConstants.TraceIdKey] = applicationTraceId;
    }

    private void WriteOriginalTraceIdToLogs(int statusCode, string originalTraceId)
    {
        m_logger.LogInformation("Forwarding an error with status code {statusCode}.", statusCode);

        if (originalTraceId != c_invalidTraceId)
        {
            m_logger.LogTrace("Original externally-provided traceId value: \"{traceId}\".", originalTraceId);
        }
    }
}
