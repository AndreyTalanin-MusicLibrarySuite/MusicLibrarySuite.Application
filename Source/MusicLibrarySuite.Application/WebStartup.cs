using System;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;

using MusicLibrarySuite.Application.Extensions;
using MusicLibrarySuite.AspNetCore.Diagnostics;
using MusicLibrarySuite.AspNetCore.Diagnostics.Extensions;
using MusicLibrarySuite.AspNetCore.Hosting;
using MusicLibrarySuite.Infrastructure.Exceptions.Extensions;
using MusicLibrarySuite.Infrastructure.Microservices.Extensions;
using MusicLibrarySuite.MicroserviceClients.CatalogService;
using MusicLibrarySuite.MicroserviceClients.Extensions;

namespace MusicLibrarySuite.Application;

/// <summary>
/// Represents an abstraction that contains runtime-called methods like
/// <see cref="ConfigureServices(IServiceCollection)" /> or <see cref="Configure(IApplicationBuilder)" />
/// to configure the ASP.NET Core host builder and the application's HTTP request pipeline.
/// </summary>
public class WebStartup : WebStartupBase
{
    /// <summary>
    /// Initializes a new instance of the <see cref="WebStartup" /> type using the application configuration.
    /// </summary>
    /// <param name="configuration">The application configuration.</param>
    /// <param name="webHostEnvironment">The web hosting environment information provider.</param>
    public WebStartup(IConfiguration configuration, IWebHostEnvironment webHostEnvironment)
        : base(configuration, webHostEnvironment)
    {
    }

    /// <inheritdoc />
    public override void ConfigureServices(IServiceCollection services)
    {
        services.AddCmsServices();

        services.AddControllers();

        services.AddProblemDetails(options =>
        {
            options.AddMusicLibrarySuiteExceptionData(externalApplication: true);
            options.AddTraceIdData();
        });

        services.AddStatusCodeForwarding();

        services.AddExceptionHandler<InternalServerErrorExceptionHandler>();

        services.AddExceptionHandlerFeatureSource();

        services.AddEndpointsApiExplorer();

        services.AddSwaggerGen(options =>
        {
            string version = "v0.1.0";
            OpenApiContact contact = new()
            {
                Name = "Andrey Talanin",
                Email = "andrey.talanin@outlook.com",
                Url = new Uri("https://github.com/AndreyTalanin"),
            };
            OpenApiLicense license = new()
            {
                Name = "The MIT License",
                Url = new Uri("https://github.com/AndreyTalanin-MusicLibrarySuite/MusicLibrarySuite.Application/blob/main/LICENSE.md"),
            };

            options.SwaggerDoc("MusicLibrarySuite.Application", new OpenApiInfo()
            {
                Title = $"Music Library Suite - Application {version}",
                Description = "Initial pre-release (unstable) API version.",
                Version = version,
                Contact = contact,
                License = license,
            });

            options.IncludeAspNetCoreXmlComments();
            options.IncludeApplicationXmlComments();
            options.IncludeApplicationContractsXmlComments();

            options.SupportNonNullableReferenceTypes();
        });

        services.AddHttpContextAccessor();

        services.AddSpaStaticFiles(staticFilesOptions =>
        {
            // Must match the <SpaPublishRoot /> MSBuild property of the .csproj file.
            staticFilesOptions.RootPath = "./ReactSpa";
        });

        services.AddAutoMapper(options =>
        {
            options.AddCmsProfiles();
        });

        services.AddExceptionMapper();

        services.AddHttpClient();

        services.AddMicroserviceInfoProvider(this);

        services.AddMicroserviceClient<ICatalogServiceClient>();
    }

    /// <inheritdoc />
    public override void Configure(IApplicationBuilder applicationBuilder)
    {
        if (WebHostEnvironment.IsDevelopment())
        {
            applicationBuilder.UseSwagger();
            applicationBuilder.UseSwaggerUI(options =>
            {
                options.SwaggerEndpoint($"/swagger/MusicLibrarySuite.Application/swagger.json", $"MusicLibrarySuite.Application");
            });

            applicationBuilder.UseDeveloperExceptionPage();
        }
        else
        {
            applicationBuilder.UseHsts();
        }

        applicationBuilder.UseHttpsRedirection();

        applicationBuilder.UseStaticFiles();
        applicationBuilder.UseSpaStaticFiles();

        applicationBuilder.UseRouting();

        applicationBuilder.UseAuthorization();

        applicationBuilder.UseExceptionHandler();

        applicationBuilder.UseStatusCodeForwarding();

        applicationBuilder.UseEndpoints(endpointRouteBuilder =>
        {
            endpointRouteBuilder.MapControllers();
        });

        applicationBuilder.UseSpa(spaDevelopmentServerOptions =>
        {
            // Must match the <SpaRoot /> MSBuild property of the .csproj file.
            spaDevelopmentServerOptions.SourcePath = "../MusicLibrarySuite.Application.ReactSpa";
            spaDevelopmentServerOptions.PackageManagerCommand = "npm";
            spaDevelopmentServerOptions.Script = "start";
        });
    }
}
