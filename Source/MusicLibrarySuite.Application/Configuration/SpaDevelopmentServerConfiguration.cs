namespace MusicLibrarySuite.Application.Configuration;

/// <summary>
/// Represents a Single Page Application (SPA) development server configuration.
/// </summary>
public class SpaDevelopmentServerConfiguration
{
    /// <summary>
    /// Gets the configuration section name.
    /// </summary>
    public static string SectionName { get; } = "SpaDevelopmentServer";

    /// <summary>
    /// Gets or sets a value indicating whether the Single Page Application (SPA) development server proxy should be used.
    /// </summary>
    public bool UseExternalDevelopmentServer { get; set; }

    /// <summary>
    /// Gets or sets the base URI of the Single Page Application (SPA) development server to which requests should be proxied.
    /// </summary>
    public string? ExternalDevelopmentServerBaseUri { get; set; }
}
