namespace MusicLibrarySuite.Application.Configuration;

/// <summary>
/// Represents a Single Page Application (SPA) development server configuration options.
/// </summary>
public class SpaDevelopmentServerOptions
{
    /// <summary>
    /// Gets or sets the path, relative to the application working directory, of the directory that contains the Single Page Application (SPA) source files during development.
    /// The directory may not exist in published applications.
    /// </summary>
    public string SourcePath { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the name of the package manager executable, (e.g npm, yarn) to run the Single Page Application (SPA).
    /// The default value is 'npm'.
    /// </summary>
    public string PackageManagerCommand { get; set; } = "npm";

    /// <summary>
    /// Gets or sets the name of the package manager project script, (e.g start, dev) to run the Single Page Application (SPA).
    /// The default value is 'start'.
    /// </summary>
    public string Script { get; set; } = "start";
}
