using System.Threading.Tasks;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MusicLibrarySuite.Application.Controllers;

/// <summary>
/// Represents an API controller for health checks.
/// </summary>
[ApiController]
[Route("api/[controller]/[action]")]
public class HealthCheckController : Controller
{
    /// <summary>
    /// Initializes a new instance of the <see cref="HealthCheckController" /> type.
    /// </summary>
    public HealthCheckController()
    {
    }

    /// <summary>
    /// Performs a liveness health check.
    /// </summary>
    /// <returns>The task object representing the asynchronous operation.</returns>
    [HttpGet]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public Task<IActionResult> CheckLivenessAsync()
    {
        return Task.FromResult<IActionResult>(Ok());
    }

    /// <summary>
    /// Performs a readiness health check.
    /// </summary>
    /// <returns>The task object representing the asynchronous operation.</returns>
    [HttpGet]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public Task<IActionResult> CheckReadinessAsync()
    {
        return Task.FromResult<IActionResult>(Ok());
    }
}
