import { Typography } from "antd";
import { useEffect } from "react";
import { useLocation } from "react-router-dom";

const { Paragraph, Title } = Typography;

const SwaggerRedirectPage = () => {
  const location = useLocation();

  useEffect(() => {
    const swaggerLocation = `${window.location.origin}${location.pathname}`;
    window.location.replace(swaggerLocation);
  }, [location]);

  return (
    <>
      <Title level={4}>Swagger API Explorer</Title>
      <Paragraph>Redirecting to the Swagger API Explorer page...</Paragraph>
    </>
  );
};

export default SwaggerRedirectPage;
