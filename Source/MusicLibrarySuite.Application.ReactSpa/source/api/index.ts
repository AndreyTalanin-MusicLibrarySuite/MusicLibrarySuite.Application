/* tslint:disable */
/* eslint-disable */

import { MusicLibrarySuite } from "./ApplicationClient";

export class ApplicationClient extends MusicLibrarySuite.Application.Client.ApplicationClient {}
export class ApiException extends MusicLibrarySuite.Application.Client.ApiException {}
