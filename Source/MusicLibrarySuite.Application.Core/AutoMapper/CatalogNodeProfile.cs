using AutoMapper;

using MusicLibrarySuite.CatalogService.Contracts.Models;

// Disable the IDE0079 (Remove unnecessary suppression) notification due to false positive alerts.
#pragma warning disable IDE0079

namespace MusicLibrarySuite.Application.Core.AutoMapper;

/// <summary>
/// Represents a service-layer AutoMapper profile for the <see cref="CatalogNodeModel" /> service model.
/// </summary>
public class CatalogNodeProfile : Profile
{
    /// <summary>
    /// Initializes a new instance of the <see cref="CatalogNodeProfile" /> type.
    /// </summary>
    public CatalogNodeProfile()
    {
        SourceMemberNamingConvention = ExactMatchNamingConvention.Instance;
        DestinationMemberNamingConvention = ExactMatchNamingConvention.Instance;

#pragma warning disable IDE0001
        CreateMap<
            MusicLibrarySuite.CatalogService.Contracts.Models.CatalogNodeModel,
            MusicLibrarySuite.Application.Contracts.Models.CatalogNodeModel>()
            .ReverseMap();
        CreateMap<
            MusicLibrarySuite.CatalogService.Contracts.Models.CatalogNodeToCatalogRelationshipModel,
            MusicLibrarySuite.Application.Contracts.Models.CatalogNodeToCatalogRelationshipModel>()
            .ReverseMap();
        CreateMap<
            MusicLibrarySuite.CatalogService.Contracts.Models.CatalogNodeHierarchicalRelationshipModel,
            MusicLibrarySuite.Application.Contracts.Models.CatalogNodeHierarchicalRelationshipModel>()
            .ReverseMap();
        CreateMap<
            MusicLibrarySuite.CatalogService.Contracts.Models.CatalogNodeRelationshipModel,
            MusicLibrarySuite.Application.Contracts.Models.CatalogNodeRelationshipModel>()
            .ReverseMap();
#pragma warning restore IDE0001
    }
}
