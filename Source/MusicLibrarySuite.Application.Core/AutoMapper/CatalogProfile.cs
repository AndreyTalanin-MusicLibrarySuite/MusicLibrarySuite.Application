using AutoMapper;

using MusicLibrarySuite.CatalogService.Contracts.Models;

// Disable the IDE0079 (Remove unnecessary suppression) notification due to false positive alerts.
#pragma warning disable IDE0079

namespace MusicLibrarySuite.Application.Core.AutoMapper;

/// <summary>
/// Represents a service-layer AutoMapper profile for the <see cref="CatalogModel" /> service model.
/// </summary>
public class CatalogProfile : Profile
{
    /// <summary>
    /// Initializes a new instance of the <see cref="CatalogProfile" /> type.
    /// </summary>
    public CatalogProfile()
    {
        SourceMemberNamingConvention = ExactMatchNamingConvention.Instance;
        DestinationMemberNamingConvention = ExactMatchNamingConvention.Instance;

#pragma warning disable IDE0001
        CreateMap<
            MusicLibrarySuite.CatalogService.Contracts.Models.CatalogModel,
            MusicLibrarySuite.Application.Contracts.Models.CatalogModel>()
            .ReverseMap();
        CreateMap<
            MusicLibrarySuite.CatalogService.Contracts.Models.CatalogRelationshipModel,
            MusicLibrarySuite.Application.Contracts.Models.CatalogRelationshipModel>()
            .ReverseMap();
#pragma warning restore IDE0001
    }
}
