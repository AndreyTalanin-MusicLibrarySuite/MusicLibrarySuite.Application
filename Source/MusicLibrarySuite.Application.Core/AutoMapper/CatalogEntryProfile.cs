using AutoMapper;

using MusicLibrarySuite.CatalogService.Contracts.Models;

// Disable the IDE0079 (Remove unnecessary suppression) notification due to false positive alerts.
#pragma warning disable IDE0079

namespace MusicLibrarySuite.Application.Core.AutoMapper;

/// <summary>
/// Represents a service-layer AutoMapper profile for the <see cref="CatalogEntryModel" /> service model.
/// </summary>
public class CatalogEntryProfile : Profile
{
    /// <summary>
    /// Initializes a new instance of the <see cref="CatalogEntryProfile" /> type.
    /// </summary>
    public CatalogEntryProfile()
    {
        SourceMemberNamingConvention = ExactMatchNamingConvention.Instance;
        DestinationMemberNamingConvention = ExactMatchNamingConvention.Instance;

#pragma warning disable IDE0001
        CreateMap<
            MusicLibrarySuite.CatalogService.Contracts.Models.CatalogEntryModel,
            MusicLibrarySuite.Application.Contracts.Models.CatalogEntryModel>()
            .ReverseMap();
        CreateMap<
            MusicLibrarySuite.CatalogService.Contracts.Models.CatalogEntryToCatalogRelationshipModel,
            MusicLibrarySuite.Application.Contracts.Models.CatalogEntryToCatalogRelationshipModel>()
            .ReverseMap();
        CreateMap<
            MusicLibrarySuite.CatalogService.Contracts.Models.CatalogEntryToCatalogNodeRelationshipModel,
            MusicLibrarySuite.Application.Contracts.Models.CatalogEntryToCatalogNodeRelationshipModel>()
            .ReverseMap();
        CreateMap<
            MusicLibrarySuite.CatalogService.Contracts.Models.CatalogEntryHierarchicalRelationshipModel,
            MusicLibrarySuite.Application.Contracts.Models.CatalogEntryHierarchicalRelationshipModel>()
            .ReverseMap();
        CreateMap<
            MusicLibrarySuite.CatalogService.Contracts.Models.CatalogEntryHierarchicalRelationshipAnnotationModel,
            MusicLibrarySuite.Application.Contracts.Models.CatalogEntryHierarchicalRelationshipAnnotationModel>()
            .ReverseMap();
        CreateMap<
            MusicLibrarySuite.CatalogService.Contracts.Models.CatalogEntryHierarchicalRelationshipTypeModel,
            MusicLibrarySuite.Application.Contracts.Models.CatalogEntryHierarchicalRelationshipTypeModel>()
            .ReverseMap();
        CreateMap<
            MusicLibrarySuite.CatalogService.Contracts.Models.CatalogEntryRelationshipModel,
            MusicLibrarySuite.Application.Contracts.Models.CatalogEntryRelationshipModel>()
            .ReverseMap();
        CreateMap<
            MusicLibrarySuite.CatalogService.Contracts.Models.CatalogEntryRelationshipAnnotationModel,
            MusicLibrarySuite.Application.Contracts.Models.CatalogEntryRelationshipAnnotationModel>()
            .ReverseMap();
        CreateMap<
            MusicLibrarySuite.CatalogService.Contracts.Models.CatalogEntryRelationshipTypeModel,
            MusicLibrarySuite.Application.Contracts.Models.CatalogEntryRelationshipTypeModel>()
            .ReverseMap();
        CreateMap<
            MusicLibrarySuite.CatalogService.Contracts.Models.CatalogEntryTypeModel,
            MusicLibrarySuite.Application.Contracts.Models.CatalogEntryTypeModel>()
            .ReverseMap();
#pragma warning restore IDE0001
    }
}
