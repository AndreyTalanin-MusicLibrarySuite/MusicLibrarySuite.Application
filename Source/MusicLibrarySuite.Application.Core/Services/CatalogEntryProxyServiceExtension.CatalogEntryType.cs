using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using MusicLibrarySuite.Application.Core.Services.Base;
using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Contracts.Models;
using MusicLibrarySuite.CatalogService.Contracts.Services.Abstractions;
using MusicLibrarySuite.Infrastructure.Exceptions;
using MusicLibrarySuite.Infrastructure.Exceptions.Services.Abstractions;
using MusicLibrarySuite.MicroserviceClients.CatalogService;
using MusicLibrarySuite.MicroserviceClients.Exceptions;

// Disable the IDE0305 (Simplify collection initialization) notification due to the confusing syntax of array initialization.
#pragma warning disable IDE0305

namespace MusicLibrarySuite.Application.Core.Services;

/// <summary>
/// Represents the proxy implementation of the service extension for the <see cref="CatalogEntryTypeModel" /> service model.
/// </summary>
public class CatalogEntryTypeProxyServiceExtension : ProxyServiceBase, ICatalogEntryTypeServiceExtension
{
    private readonly ICatalogServiceClient m_catalogServiceClient;

    /// <summary>
    /// Initializes a new instance of the <see cref="CatalogEntryTypeProxyServiceExtension" /> type using the specified services.
    /// </summary>
    /// <param name="catalogServiceClient">The <c>MusicLibrarySuite.CatalogService</c> microservice client.</param>
    /// <param name="exceptionMapperProxy">The exception mapper proxy that performs exception-prone calls and conditionally maps thrown exceptions.</param>
    public CatalogEntryTypeProxyServiceExtension(ICatalogServiceClient catalogServiceClient, IExceptionMapperProxy exceptionMapperProxy)
        : base(exceptionMapperProxy)
    {
        m_catalogServiceClient = catalogServiceClient;
    }

    /// <inheritdoc />
    /// <exception cref="MusicLibrarySuiteForwardStatusCodeException">Thrown if an unexpected client error occurs.</exception>
    /// <exception cref="ApiException{ProblemDetails}">Thrown if an unexpected server error occurs.</exception>
    /// <exception cref="ApiException">Thrown if an unexpected server error occurs.</exception>
    public async Task<CatalogEntryTypeModel?> GetCatalogEntryTypeAsync(CatalogEntryTypeId catalogEntryTypeId, CancellationToken cancellationToken = default)
    {
        Guid catalogEntryTypeIdValue = catalogEntryTypeId.Value;
        CatalogEntryTypeModel catalogEntryTypeModel = await ExceptionMapperProxy.InvokeAsync(
            async (cancellationToken) => await m_catalogServiceClient.GetCatalogEntryTypeAsync(catalogEntryTypeIdValue, cancellationToken),
            cancellationToken);
        return catalogEntryTypeModel;
    }

    /// <inheritdoc />
    /// <exception cref="MusicLibrarySuiteForwardStatusCodeException">Thrown if an unexpected client error occurs.</exception>
    /// <exception cref="ApiException{ProblemDetails}">Thrown if an unexpected server error occurs.</exception>
    /// <exception cref="ApiException">Thrown if an unexpected server error occurs.</exception>
    public async Task<CatalogEntryTypeModel[]> GetCatalogEntryTypesAsync(IEnumerable<CatalogEntryTypeId> catalogEntryTypeIds, CancellationToken cancellationToken = default)
    {
        IEnumerable<Guid> catalogEntryTypeIdValues =
            catalogEntryTypeIds.Select(catalogEntryTypeId => catalogEntryTypeId.Value);
        ICollection<CatalogEntryTypeModel> catalogEntryTypeModels = await ExceptionMapperProxy.InvokeAsync(
            async (cancellationToken) => await m_catalogServiceClient.GetCatalogEntryTypesAsync(catalogEntryTypeIdValues, cancellationToken),
            cancellationToken);
        return catalogEntryTypeModels.ToArray();
    }

    /// <inheritdoc />
    /// <exception cref="MusicLibrarySuiteForwardStatusCodeException">Thrown if an unexpected client error occurs.</exception>
    /// <exception cref="ApiException{ProblemDetails}">Thrown if an unexpected server error occurs.</exception>
    /// <exception cref="ApiException">Thrown if an unexpected server error occurs.</exception>
    [SuppressMessage("Style", "IDE0200:Remove unnecessary lambda expression", Justification = "To increase readability for async cancellable calls.")]
    public async Task<CatalogEntryTypeModel[]> GetCatalogEntryTypesAsync(CancellationToken cancellationToken = default)
    {
        ICollection<CatalogEntryTypeModel> catalogEntryTypeModels = await ExceptionMapperProxy.InvokeAsync(
            async (cancellationToken) => await m_catalogServiceClient.GetAllCatalogEntryTypesAsync(cancellationToken),
            cancellationToken);
        return catalogEntryTypeModels.ToArray();
    }
}
