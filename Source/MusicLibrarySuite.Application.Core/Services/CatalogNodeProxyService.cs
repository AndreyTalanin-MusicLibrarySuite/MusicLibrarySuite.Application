using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Http;

using MusicLibrarySuite.Application.Core.Services.Base;
using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Contracts.Models;
using MusicLibrarySuite.CatalogService.Contracts.Services.Abstractions;
using MusicLibrarySuite.Infrastructure.Exceptions;
using MusicLibrarySuite.Infrastructure.Exceptions.Services.Abstractions;
using MusicLibrarySuite.MicroserviceClients.CatalogService;
using MusicLibrarySuite.MicroserviceClients.Exceptions;

// Disable the IDE0305 (Simplify collection initialization) notification due to the confusing syntax of array initialization.
#pragma warning disable IDE0305

namespace MusicLibrarySuite.Application.Core.Services;

/// <summary>
/// Represents the proxy implementation of the catalog node service.
/// </summary>
public class CatalogNodeProxyService : ProxyServiceBase, ICatalogNodeService
{
    private readonly ICatalogServiceClient m_catalogServiceClient;

    /// <summary>
    /// Initializes a new instance of the <see cref="CatalogNodeProxyService" /> type using the specified services.
    /// </summary>
    /// <param name="catalogServiceClient">The <c>MusicLibrarySuite.CatalogService</c> microservice client.</param>
    /// <param name="exceptionMapperProxy">The exception mapper proxy that performs exception-prone calls and conditionally maps thrown exceptions.</param>
    public CatalogNodeProxyService(ICatalogServiceClient catalogServiceClient, IExceptionMapperProxy exceptionMapperProxy)
        : base(exceptionMapperProxy)
    {
        m_catalogServiceClient = catalogServiceClient;
    }

    /// <inheritdoc />
    /// <exception cref="MusicLibrarySuiteForwardStatusCodeException">Thrown if an unexpected client error occurs.</exception>
    /// <exception cref="ApiException{ProblemDetails}">Thrown if an unexpected server error occurs.</exception>
    /// <exception cref="ApiException">Thrown if an unexpected server error occurs.</exception>
    public async Task<CatalogNodeModel?> GetCatalogNodeAsync(CatalogNodeId catalogNodeId, CancellationToken cancellationToken = default)
    {
        Guid catalogNodeIdValue = catalogNodeId.Value;
        CatalogNodeModel? catalogNodeModel = await ExceptionMapperProxy.InvokeAsync(async (cancellationToken) =>
        {
            try
            {
                return await m_catalogServiceClient.GetCatalogNodeAsync(catalogNodeIdValue, cancellationToken);
            }
            catch (ApiException apiException) when (apiException.StatusCode == StatusCodes.Status404NotFound)
            {
                return null;
            }
        }, cancellationToken);
        return catalogNodeModel;
    }

    /// <inheritdoc />
    /// <exception cref="MusicLibrarySuiteForwardStatusCodeException">Thrown if an unexpected client error occurs.</exception>
    /// <exception cref="ApiException{ProblemDetails}">Thrown if an unexpected server error occurs.</exception>
    /// <exception cref="ApiException">Thrown if an unexpected server error occurs.</exception>
    public async Task<CatalogNodeModel[]> GetCatalogNodesAsync(IEnumerable<CatalogNodeId> catalogNodeIds, CancellationToken cancellationToken = default)
    {
        IEnumerable<Guid> catalogNodeIdValues = catalogNodeIds.Select(catalogNodeId => catalogNodeId.Value);
        ICollection<CatalogNodeModel> catalogNodeModels = await ExceptionMapperProxy.InvokeAsync(
            async (cancellationToken) => await m_catalogServiceClient.GetCatalogNodesAsync(catalogNodeIdValues, cancellationToken),
            cancellationToken);
        return catalogNodeModels.ToArray();
    }

    /// <inheritdoc />
    /// <exception cref="MusicLibrarySuiteForwardStatusCodeException">Thrown if an unexpected client error occurs.</exception>
    /// <exception cref="ApiException{ProblemDetails}">Thrown if an unexpected server error occurs.</exception>
    /// <exception cref="ApiException">Thrown if an unexpected server error occurs.</exception>
    [SuppressMessage("Style", "IDE0200:Remove unnecessary lambda expression", Justification = "To increase readability for async cancellable calls.")]
    public async Task<CatalogNodeModel[]> GetCatalogNodesAsync(CancellationToken cancellationToken = default)
    {
        ICollection<CatalogNodeModel> catalogNodeModels = await ExceptionMapperProxy.InvokeAsync(
            async (cancellationToken) => await m_catalogServiceClient.GetAllCatalogNodesAsync(cancellationToken),
            cancellationToken);
        return catalogNodeModels.ToArray();
    }

    /// <inheritdoc />
    /// <exception cref="MusicLibrarySuiteForwardStatusCodeException">Thrown if an unexpected client error occurs.</exception>
    /// <exception cref="ApiException{ProblemDetails}">Thrown if an unexpected server error occurs.</exception>
    /// <exception cref="ApiException">Thrown if an unexpected server error occurs.</exception>
    [SuppressMessage("Style", "IDE0200:Remove unnecessary lambda expression", Justification = "To increase readability for async cancellable calls.")]
    public async Task<int> CountCatalogNodesAsync(CancellationToken cancellationToken = default)
    {
        int catalogNodesCount = await ExceptionMapperProxy.InvokeAsync(
            async (cancellationToken) => await m_catalogServiceClient.CountAllCatalogNodesAsync(cancellationToken),
            cancellationToken);
        return catalogNodesCount;
    }

    /// <inheritdoc />
    /// <exception cref="MusicLibrarySuiteForwardStatusCodeException">Thrown if an unexpected client error occurs.</exception>
    /// <exception cref="ApiException{ProblemDetails}">Thrown if an unexpected server error occurs.</exception>
    /// <exception cref="ApiException">Thrown if an unexpected server error occurs.</exception>
    public async Task<CatalogNodeModel> AddCatalogNodeAsync(CatalogNodeModel catalogNodeModel, CancellationToken cancellationToken = default)
    {
        CatalogNodeModel addedCatalogNodeModel = await ExceptionMapperProxy.InvokeAsync(
            async (cancellationToken) => await m_catalogServiceClient.AddCatalogNodeAsync(catalogNodeModel, cancellationToken),
            cancellationToken);
        return addedCatalogNodeModel;
    }

    /// <inheritdoc />
    /// <exception cref="MusicLibrarySuiteForwardStatusCodeException">Thrown if an unexpected client error occurs.</exception>
    /// <exception cref="ApiException{ProblemDetails}">Thrown if an unexpected server error occurs.</exception>
    /// <exception cref="ApiException">Thrown if an unexpected server error occurs.</exception>
    public async Task UpdateCatalogNodeAsync(CatalogNodeModel catalogNodeModel, CancellationToken cancellationToken = default)
    {
        await ExceptionMapperProxy.InvokeAsync(
            async (cancellationToken) => await m_catalogServiceClient.UpdateCatalogNodeAsync(catalogNodeModel, cancellationToken),
            cancellationToken);
    }

    /// <inheritdoc />
    /// <exception cref="MusicLibrarySuiteForwardStatusCodeException">Thrown if an unexpected client error occurs.</exception>
    /// <exception cref="ApiException{ProblemDetails}">Thrown if an unexpected server error occurs.</exception>
    /// <exception cref="ApiException">Thrown if an unexpected server error occurs.</exception>
    public async Task RemoveCatalogNodeAsync(CatalogNodeId catalogNodeId, CancellationToken cancellationToken = default)
    {
        Guid catalogNodeIdValue = catalogNodeId.Value;
        await ExceptionMapperProxy.InvokeAsync(
            async (cancellationToken) => await m_catalogServiceClient.RemoveCatalogNodeAsync(catalogNodeIdValue, cancellationToken),
            cancellationToken);
    }
}
