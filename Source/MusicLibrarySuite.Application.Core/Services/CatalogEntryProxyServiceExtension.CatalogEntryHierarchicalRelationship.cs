using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using MusicLibrarySuite.Application.Core.Services.Base;
using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Contracts.Models;
using MusicLibrarySuite.CatalogService.Contracts.Services.Abstractions;
using MusicLibrarySuite.Infrastructure.Exceptions;
using MusicLibrarySuite.Infrastructure.Exceptions.Services.Abstractions;
using MusicLibrarySuite.MicroserviceClients.CatalogService;
using MusicLibrarySuite.MicroserviceClients.Exceptions;

// Disable the IDE0305 (Simplify collection initialization) notification due to the confusing syntax of array initialization.
#pragma warning disable IDE0305

namespace MusicLibrarySuite.Application.Core.Services;

/// <summary>
/// Represents the proxy implementation of the service extension for the <see cref="CatalogEntryHierarchicalRelationshipModel" /> service model.
/// </summary>
public class CatalogEntryHierarchicalRelationshipProxyServiceExtension : ProxyServiceBase, ICatalogEntryHierarchicalRelationshipServiceExtension
{
    private readonly ICatalogServiceClient m_catalogServiceClient;

    /// <summary>
    /// Initializes a new instance of the <see cref="CatalogEntryHierarchicalRelationshipProxyServiceExtension" /> type using the specified services.
    /// </summary>
    /// <param name="catalogServiceClient">The <c>MusicLibrarySuite.CatalogService</c> microservice client.</param>
    /// <param name="exceptionMapperProxy">The exception mapper proxy that performs exception-prone calls and conditionally maps thrown exceptions.</param>
    public CatalogEntryHierarchicalRelationshipProxyServiceExtension(ICatalogServiceClient catalogServiceClient, IExceptionMapperProxy exceptionMapperProxy)
        : base(exceptionMapperProxy)
    {
        m_catalogServiceClient = catalogServiceClient;
    }

    /// <inheritdoc />
    /// <exception cref="MusicLibrarySuiteForwardStatusCodeException">Thrown if an unexpected client error occurs.</exception>
    /// <exception cref="ApiException{ProblemDetails}">Thrown if an unexpected server error occurs.</exception>
    /// <exception cref="ApiException">Thrown if an unexpected server error occurs.</exception>
    public async Task<CatalogEntryHierarchicalRelationshipModel[]> GetCatalogEntryHierarchicalRelationshipsAsync(CatalogEntryId catalogEntryId, bool byTarget, CancellationToken cancellationToken = default)
    {
        Guid catalogEntryIdValue = catalogEntryId.Value;
        ICollection<CatalogEntryHierarchicalRelationshipModel> catalogEntryHierarchicalRelationshipModels = await ExceptionMapperProxy.InvokeAsync(
            async (cancellationToken) => await m_catalogServiceClient.GetCatalogEntryHierarchicalRelationshipsAsync(
                catalogEntryId: catalogEntryIdValue,
                catalogEntryHierarchicalRelationshipTypeId: null,
                byTarget,
                cancellationToken),
            cancellationToken);
        return catalogEntryHierarchicalRelationshipModels.ToArray();
    }

    /// <inheritdoc />
    /// <exception cref="MusicLibrarySuiteForwardStatusCodeException">Thrown if an unexpected client error occurs.</exception>
    /// <exception cref="ApiException{ProblemDetails}">Thrown if an unexpected server error occurs.</exception>
    /// <exception cref="ApiException">Thrown if an unexpected server error occurs.</exception>
    public async Task<CatalogEntryHierarchicalRelationshipModel[]> GetCatalogEntryHierarchicalRelationshipsAsync(CatalogEntryId catalogEntryId, CatalogEntryHierarchicalRelationshipTypeId? catalogEntryHierarchicalRelationshipTypeId, bool byTarget, CancellationToken cancellationToken = default)
    {
        Guid catalogEntryIdValue = catalogEntryId.Value;
        Guid? catalogEntryHierarchicalRelationshipTypeIdValue = catalogEntryHierarchicalRelationshipTypeId?.Value;
        ICollection<CatalogEntryHierarchicalRelationshipModel> catalogEntryHierarchicalRelationshipModels = await ExceptionMapperProxy.InvokeAsync(
            async (cancellationToken) => await m_catalogServiceClient.GetCatalogEntryHierarchicalRelationshipsAsync(
                catalogEntryId: catalogEntryIdValue,
                catalogEntryHierarchicalRelationshipTypeId: catalogEntryHierarchicalRelationshipTypeIdValue,
                byTarget,
                cancellationToken),
            cancellationToken);
        return catalogEntryHierarchicalRelationshipModels.ToArray();
    }

    /// <inheritdoc />
    /// <exception cref="MusicLibrarySuiteForwardStatusCodeException">Thrown if an unexpected client error occurs.</exception>
    /// <exception cref="ApiException{ProblemDetails}">Thrown if an unexpected server error occurs.</exception>
    /// <exception cref="ApiException">Thrown if an unexpected server error occurs.</exception>
    public async Task ReorderCatalogEntryHierarchicalRelationshipsAsync(CatalogEntryId catalogEntryId, IEnumerable<CatalogEntryHierarchicalRelationshipModel> catalogEntryHierarchicalRelationshipModels, bool byTarget, CancellationToken cancellationToken = default)
    {
        Guid catalogEntryIdValue = catalogEntryId.Value;
        await ExceptionMapperProxy.InvokeAsync(
            async (cancellationToken) => await m_catalogServiceClient.ReorderCatalogEntryHierarchicalRelationshipsAsync(catalogEntryIdValue, byTarget, catalogEntryHierarchicalRelationshipModels, cancellationToken),
            cancellationToken);
    }
}
