using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using MusicLibrarySuite.Application.Core.Services.Base;
using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Contracts.Models;
using MusicLibrarySuite.CatalogService.Contracts.Services.Abstractions;
using MusicLibrarySuite.Infrastructure.Exceptions;
using MusicLibrarySuite.Infrastructure.Exceptions.Services.Abstractions;
using MusicLibrarySuite.MicroserviceClients.CatalogService;
using MusicLibrarySuite.MicroserviceClients.Exceptions;

// Disable the IDE0305 (Simplify collection initialization) notification due to the confusing syntax of array initialization.
#pragma warning disable IDE0305

namespace MusicLibrarySuite.Application.Core.Services;

/// <summary>
/// Represents the proxy implementation of the service extension for the <see cref="CatalogEntryRelationshipModel" /> service model.
/// </summary>
public class CatalogEntryRelationshipProxyServiceExtension : ProxyServiceBase, ICatalogEntryRelationshipServiceExtension
{
    private readonly ICatalogServiceClient m_catalogServiceClient;

    /// <summary>
    /// Initializes a new instance of the <see cref="CatalogEntryRelationshipProxyServiceExtension" /> type using the specified services.
    /// </summary>
    /// <param name="catalogServiceClient">The <c>MusicLibrarySuite.CatalogService</c> microservice client.</param>
    /// <param name="exceptionMapperProxy">The exception mapper proxy that performs exception-prone calls and conditionally maps thrown exceptions.</param>
    public CatalogEntryRelationshipProxyServiceExtension(ICatalogServiceClient catalogServiceClient, IExceptionMapperProxy exceptionMapperProxy)
        : base(exceptionMapperProxy)
    {
        m_catalogServiceClient = catalogServiceClient;
    }

    /// <inheritdoc />
    /// <exception cref="MusicLibrarySuiteForwardStatusCodeException">Thrown if an unexpected client error occurs.</exception>
    /// <exception cref="ApiException{ProblemDetails}">Thrown if an unexpected server error occurs.</exception>
    /// <exception cref="ApiException">Thrown if an unexpected server error occurs.</exception>
    public async Task<CatalogEntryRelationshipModel[]> GetCatalogEntryRelationshipsAsync(CatalogEntryId catalogEntryId, bool byTarget, CancellationToken cancellationToken = default)
    {
        Guid catalogEntryIdValue = catalogEntryId.Value;
        ICollection<CatalogEntryRelationshipModel> catalogEntryRelationshipModels = await ExceptionMapperProxy.InvokeAsync(
            async (cancellationToken) => await m_catalogServiceClient.GetCatalogEntryRelationshipsAsync(
                catalogEntryId: catalogEntryIdValue,
                catalogEntryRelationshipTypeId: null,
                byTarget,
                cancellationToken),
            cancellationToken);
        return catalogEntryRelationshipModels.ToArray();
    }

    /// <inheritdoc />
    /// <exception cref="MusicLibrarySuiteForwardStatusCodeException">Thrown if an unexpected client error occurs.</exception>
    /// <exception cref="ApiException{ProblemDetails}">Thrown if an unexpected server error occurs.</exception>
    /// <exception cref="ApiException">Thrown if an unexpected server error occurs.</exception>
    public async Task<CatalogEntryRelationshipModel[]> GetCatalogEntryRelationshipsAsync(CatalogEntryId catalogEntryId, CatalogEntryRelationshipTypeId? catalogEntryRelationshipTypeId, bool byTarget, CancellationToken cancellationToken = default)
    {
        Guid catalogEntryIdValue = catalogEntryId.Value;
        Guid? catalogEntryRelationshipTypeIdValue = catalogEntryRelationshipTypeId?.Value;
        ICollection<CatalogEntryRelationshipModel> catalogEntryRelationshipModels = await ExceptionMapperProxy.InvokeAsync(
            async (cancellationToken) => await m_catalogServiceClient.GetCatalogEntryRelationshipsAsync(
                catalogEntryId: catalogEntryIdValue,
                catalogEntryRelationshipTypeId: catalogEntryRelationshipTypeIdValue,
                byTarget,
                cancellationToken),
            cancellationToken);
        return catalogEntryRelationshipModels.ToArray();
    }

    /// <inheritdoc />
    /// <exception cref="MusicLibrarySuiteForwardStatusCodeException">Thrown if an unexpected client error occurs.</exception>
    /// <exception cref="ApiException{ProblemDetails}">Thrown if an unexpected server error occurs.</exception>
    /// <exception cref="ApiException">Thrown if an unexpected server error occurs.</exception>
    public async Task ReorderCatalogEntryRelationshipsAsync(CatalogEntryId catalogEntryId, IEnumerable<CatalogEntryRelationshipModel> catalogEntryRelationshipModels, bool byTarget, CancellationToken cancellationToken = default)
    {
        Guid catalogEntryIdValue = catalogEntryId.Value;
        await ExceptionMapperProxy.InvokeAsync(
            async (cancellationToken) => await m_catalogServiceClient.ReorderCatalogEntryRelationshipsAsync(catalogEntryIdValue, byTarget, catalogEntryRelationshipModels, cancellationToken),
            cancellationToken);
    }
}
