using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using MusicLibrarySuite.Application.Core.Services.Base;
using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Contracts.Models;
using MusicLibrarySuite.CatalogService.Contracts.Services.Abstractions;
using MusicLibrarySuite.Infrastructure.Exceptions;
using MusicLibrarySuite.Infrastructure.Exceptions.Services.Abstractions;
using MusicLibrarySuite.MicroserviceClients.CatalogService;
using MusicLibrarySuite.MicroserviceClients.Exceptions;

// Disable the IDE0305 (Simplify collection initialization) notification due to the confusing syntax of array initialization.
#pragma warning disable IDE0305

namespace MusicLibrarySuite.Application.Core.Services;

/// <summary>
/// Represents the proxy implementation of the service extension for the <see cref="CatalogEntryHierarchicalRelationshipTypeModel" /> service model.
/// </summary>
public class CatalogEntryHierarchicalRelationshipTypeProxyServiceExtension : ProxyServiceBase, ICatalogEntryHierarchicalRelationshipTypeServiceExtension
{
    private readonly ICatalogServiceClient m_catalogServiceClient;

    /// <summary>
    /// Initializes a new instance of the <see cref="CatalogEntryHierarchicalRelationshipTypeProxyServiceExtension" /> type using the specified services.
    /// </summary>
    /// <param name="catalogServiceClient">The <c>MusicLibrarySuite.CatalogService</c> microservice client.</param>
    /// <param name="exceptionMapperProxy">The exception mapper proxy that performs exception-prone calls and conditionally maps thrown exceptions.</param>
    public CatalogEntryHierarchicalRelationshipTypeProxyServiceExtension(ICatalogServiceClient catalogServiceClient, IExceptionMapperProxy exceptionMapperProxy)
        : base(exceptionMapperProxy)
    {
        m_catalogServiceClient = catalogServiceClient;
    }

    /// <inheritdoc />
    /// <exception cref="MusicLibrarySuiteForwardStatusCodeException">Thrown if an unexpected client error occurs.</exception>
    /// <exception cref="ApiException{ProblemDetails}">Thrown if an unexpected server error occurs.</exception>
    /// <exception cref="ApiException">Thrown if an unexpected server error occurs.</exception>
    public async Task<CatalogEntryHierarchicalRelationshipTypeModel?> GetCatalogEntryHierarchicalRelationshipTypeAsync(CatalogEntryHierarchicalRelationshipTypeId catalogEntryHierarchicalRelationshipTypeId, CancellationToken cancellationToken = default)
    {
        Guid catalogEntryHierarchicalRelationshipTypeIdValue = catalogEntryHierarchicalRelationshipTypeId.Value;
        CatalogEntryHierarchicalRelationshipTypeModel catalogEntryHierarchicalRelationshipTypeModel = await ExceptionMapperProxy.InvokeAsync(
            async (cancellationToken) => await m_catalogServiceClient.GetCatalogEntryHierarchicalRelationshipTypeAsync(catalogEntryHierarchicalRelationshipTypeIdValue, cancellationToken),
            cancellationToken);
        return catalogEntryHierarchicalRelationshipTypeModel;
    }

    /// <inheritdoc />
    /// <exception cref="MusicLibrarySuiteForwardStatusCodeException">Thrown if an unexpected client error occurs.</exception>
    /// <exception cref="ApiException{ProblemDetails}">Thrown if an unexpected server error occurs.</exception>
    /// <exception cref="ApiException">Thrown if an unexpected server error occurs.</exception>
    public async Task<CatalogEntryHierarchicalRelationshipTypeModel[]> GetCatalogEntryHierarchicalRelationshipTypesAsync(IEnumerable<CatalogEntryHierarchicalRelationshipTypeId> catalogEntryHierarchicalRelationshipTypeIds, CancellationToken cancellationToken = default)
    {
        IEnumerable<Guid> catalogEntryHierarchicalRelationshipTypeIdValues =
            catalogEntryHierarchicalRelationshipTypeIds.Select(catalogEntryHierarchicalRelationshipTypeId => catalogEntryHierarchicalRelationshipTypeId.Value);
        ICollection<CatalogEntryHierarchicalRelationshipTypeModel> catalogEntryHierarchicalRelationshipTypeModels = await ExceptionMapperProxy.InvokeAsync(
            async (cancellationToken) => await m_catalogServiceClient.GetCatalogEntryHierarchicalRelationshipTypesAsync(catalogEntryHierarchicalRelationshipTypeIdValues, cancellationToken),
            cancellationToken);
        return catalogEntryHierarchicalRelationshipTypeModels.ToArray();
    }

    /// <inheritdoc />
    /// <exception cref="MusicLibrarySuiteForwardStatusCodeException">Thrown if an unexpected client error occurs.</exception>
    /// <exception cref="ApiException{ProblemDetails}">Thrown if an unexpected server error occurs.</exception>
    /// <exception cref="ApiException">Thrown if an unexpected server error occurs.</exception>
    [SuppressMessage("Style", "IDE0200:Remove unnecessary lambda expression", Justification = "To increase readability for async cancellable calls.")]
    public async Task<CatalogEntryHierarchicalRelationshipTypeModel[]> GetCatalogEntryHierarchicalRelationshipTypesAsync(CancellationToken cancellationToken = default)
    {
        ICollection<CatalogEntryHierarchicalRelationshipTypeModel> catalogEntryHierarchicalRelationshipTypeModels = await ExceptionMapperProxy.InvokeAsync(
            async (cancellationToken) => await m_catalogServiceClient.GetAllCatalogEntryHierarchicalRelationshipTypesAsync(cancellationToken),
            cancellationToken);
        return catalogEntryHierarchicalRelationshipTypeModels.ToArray();
    }
}
