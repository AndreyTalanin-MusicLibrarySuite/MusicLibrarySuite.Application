using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using MusicLibrarySuite.Application.Core.Services.Base;
using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Contracts.Models;
using MusicLibrarySuite.CatalogService.Contracts.Services.Abstractions;
using MusicLibrarySuite.Infrastructure.Exceptions;
using MusicLibrarySuite.Infrastructure.Exceptions.Services.Abstractions;
using MusicLibrarySuite.MicroserviceClients.CatalogService;
using MusicLibrarySuite.MicroserviceClients.Exceptions;

// Disable the IDE0305 (Simplify collection initialization) notification due to the confusing syntax of array initialization.
#pragma warning disable IDE0305

namespace MusicLibrarySuite.Application.Core.Services;

/// <summary>
/// Represents the proxy implementation of the service extension for the <see cref="CatalogEntryToCatalogRelationshipModel" /> service model.
/// </summary>
public class CatalogEntryToCatalogRelationshipProxyServiceExtension : ProxyServiceBase, ICatalogEntryToCatalogRelationshipServiceExtension
{
    private readonly ICatalogServiceClient m_catalogServiceClient;

    /// <summary>
    /// Initializes a new instance of the <see cref="CatalogEntryToCatalogRelationshipProxyServiceExtension" /> type using the specified services.
    /// </summary>
    /// <param name="catalogServiceClient">The <c>MusicLibrarySuite.CatalogService</c> microservice client.</param>
    /// <param name="exceptionMapperProxy">The exception mapper proxy that performs exception-prone calls and conditionally maps thrown exceptions.</param>
    public CatalogEntryToCatalogRelationshipProxyServiceExtension(ICatalogServiceClient catalogServiceClient, IExceptionMapperProxy exceptionMapperProxy)
        : base(exceptionMapperProxy)
    {
        m_catalogServiceClient = catalogServiceClient;
    }

    /// <inheritdoc />
    /// <exception cref="MusicLibrarySuiteForwardStatusCodeException">Thrown if an unexpected client error occurs.</exception>
    /// <exception cref="ApiException{ProblemDetails}">Thrown if an unexpected server error occurs.</exception>
    /// <exception cref="ApiException">Thrown if an unexpected server error occurs.</exception>
    public async Task<CatalogEntryToCatalogRelationshipModel[]> GetCatalogEntryToCatalogRelationshipsAsync(CatalogEntryId catalogEntryId, CancellationToken cancellationToken = default)
    {
        Guid catalogEntryIdValue = catalogEntryId.Value;
        ICollection<CatalogEntryToCatalogRelationshipModel> catalogEntryToCatalogRelationshipModels = await ExceptionMapperProxy.InvokeAsync(
            async (cancellationToken) => await m_catalogServiceClient.GetCatalogEntryToCatalogRelationshipsAsync(
                catalogEntryId: catalogEntryIdValue,
                catalogId: null,
                byTarget: false,
                cancellationToken),
            cancellationToken);
        return catalogEntryToCatalogRelationshipModels.ToArray();
    }

    /// <inheritdoc />
    /// <exception cref="MusicLibrarySuiteForwardStatusCodeException">Thrown if an unexpected client error occurs.</exception>
    /// <exception cref="ApiException{ProblemDetails}">Thrown if an unexpected server error occurs.</exception>
    /// <exception cref="ApiException">Thrown if an unexpected server error occurs.</exception>
    public async Task<CatalogEntryToCatalogRelationshipModel[]> GetCatalogEntryToCatalogRelationshipsAsync(CatalogId catalogId, CancellationToken cancellationToken = default)
    {
        Guid catalogIdValue = catalogId.Value;
        ICollection<CatalogEntryToCatalogRelationshipModel> catalogEntryToCatalogRelationshipModels = await ExceptionMapperProxy.InvokeAsync(
            async (cancellationToken) => await m_catalogServiceClient.GetCatalogEntryToCatalogRelationshipsAsync(
                catalogEntryId: null,
                catalogId: catalogIdValue,
                byTarget: true,
                cancellationToken),
            cancellationToken);
        return catalogEntryToCatalogRelationshipModels.ToArray();
    }

    /// <inheritdoc />
    /// <exception cref="MusicLibrarySuiteForwardStatusCodeException">Thrown if an unexpected client error occurs.</exception>
    /// <exception cref="ApiException{ProblemDetails}">Thrown if an unexpected server error occurs.</exception>
    /// <exception cref="ApiException">Thrown if an unexpected server error occurs.</exception>
    public async Task ReorderCatalogEntryToCatalogRelationshipsAsync(CatalogEntryId catalogEntryId, IEnumerable<CatalogEntryToCatalogRelationshipModel> catalogEntryToCatalogRelationshipModels, CancellationToken cancellationToken = default)
    {
        Guid catalogEntryIdValue = catalogEntryId.Value;
        await ExceptionMapperProxy.InvokeAsync(
            async (cancellationToken) => await m_catalogServiceClient.ReorderCatalogEntryToCatalogRelationshipsAsync(
                catalogEntryId: catalogEntryIdValue,
                catalogId: null,
                byTarget: false,
                catalogEntryToCatalogRelationshipModels,
                cancellationToken),
            cancellationToken);
    }

    /// <inheritdoc />
    /// <exception cref="MusicLibrarySuiteForwardStatusCodeException">Thrown if an unexpected client error occurs.</exception>
    /// <exception cref="ApiException{ProblemDetails}">Thrown if an unexpected server error occurs.</exception>
    /// <exception cref="ApiException">Thrown if an unexpected server error occurs.</exception>
    public async Task ReorderCatalogEntryToCatalogRelationshipsAsync(CatalogId catalogId, IEnumerable<CatalogEntryToCatalogRelationshipModel> catalogEntryToCatalogRelationshipModels, CancellationToken cancellationToken = default)
    {
        Guid catalogIdValue = catalogId.Value;
        await ExceptionMapperProxy.InvokeAsync(
            async (cancellationToken) => await m_catalogServiceClient.ReorderCatalogEntryToCatalogRelationshipsAsync(
                catalogEntryId: null,
                catalogId: catalogIdValue,
                byTarget: true,
                catalogEntryToCatalogRelationshipModels,
                cancellationToken),
            cancellationToken);
    }
}
