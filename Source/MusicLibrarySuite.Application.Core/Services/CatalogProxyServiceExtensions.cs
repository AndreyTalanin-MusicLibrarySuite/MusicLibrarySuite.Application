using System;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;

using MusicLibrarySuite.CatalogService.Contracts.Models;

namespace MusicLibrarySuite.Application.Core.Services;

/// <summary>
/// This is a placeholder class to provide more convenient navigation to service extensions for the <see cref="CatalogModel" /> service model.
/// <para>
/// List of available service extensions:
/// <list type="bullet">
/// <item><see cref="CatalogRelationshipProxyServiceExtension" /></item>
/// </list>
/// </para>
/// </summary>
[EditorBrowsable(EditorBrowsableState.Never)]
[Obsolete($"Do not use the {nameof(__CatalogProxyServiceExtensions)} class, it is a navigation placeholder.", error: true)]
[SuppressMessage("Style", "IDE1006:Naming Styles", Justification = "Remove the type from the list of available options in tooltips.")]
internal static class __CatalogProxyServiceExtensions
{
}
