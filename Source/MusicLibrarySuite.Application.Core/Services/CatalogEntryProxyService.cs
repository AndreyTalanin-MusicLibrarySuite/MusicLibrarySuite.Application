using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Http;

using MusicLibrarySuite.Application.Core.Services.Base;
using MusicLibrarySuite.CatalogService.Contracts.Identifiers;
using MusicLibrarySuite.CatalogService.Contracts.Models;
using MusicLibrarySuite.CatalogService.Contracts.Services.Abstractions;
using MusicLibrarySuite.Infrastructure.Exceptions;
using MusicLibrarySuite.Infrastructure.Exceptions.Services.Abstractions;
using MusicLibrarySuite.MicroserviceClients.CatalogService;
using MusicLibrarySuite.MicroserviceClients.Exceptions;

// Disable the IDE0305 (Simplify collection initialization) notification due to the confusing syntax of array initialization.
#pragma warning disable IDE0305

namespace MusicLibrarySuite.Application.Core.Services;

/// <summary>
/// Represents the proxy implementation of the catalog entry service.
/// </summary>
public class CatalogEntryProxyService : ProxyServiceBase, ICatalogEntryService
{
    private readonly ICatalogServiceClient m_catalogServiceClient;

    /// <summary>
    /// Initializes a new instance of the <see cref="CatalogEntryProxyService" /> type using the specified services.
    /// </summary>
    /// <param name="catalogServiceClient">The <c>MusicLibrarySuite.CatalogService</c> microservice client.</param>
    /// <param name="exceptionMapperProxy">The exception mapper proxy that performs exception-prone calls and conditionally maps thrown exceptions.</param>
    public CatalogEntryProxyService(ICatalogServiceClient catalogServiceClient, IExceptionMapperProxy exceptionMapperProxy)
        : base(exceptionMapperProxy)
    {
        m_catalogServiceClient = catalogServiceClient;
    }

    /// <inheritdoc />
    /// <exception cref="MusicLibrarySuiteForwardStatusCodeException">Thrown if an unexpected client error occurs.</exception>
    /// <exception cref="ApiException{ProblemDetails}">Thrown if an unexpected server error occurs.</exception>
    /// <exception cref="ApiException">Thrown if an unexpected server error occurs.</exception>
    public async Task<CatalogEntryModel?> GetCatalogEntryAsync(CatalogEntryId catalogEntryId, CancellationToken cancellationToken = default)
    {
        Guid catalogEntryIdValue = catalogEntryId.Value;
        CatalogEntryModel? catalogEntryModel = await ExceptionMapperProxy.InvokeAsync(async (cancellationToken) =>
        {
            try
            {
                return await m_catalogServiceClient.GetCatalogEntryAsync(catalogEntryIdValue, cancellationToken);
            }
            catch (ApiException apiException) when (apiException.StatusCode == StatusCodes.Status404NotFound)
            {
                return null;
            }
        }, cancellationToken);
        return catalogEntryModel;
    }

    /// <inheritdoc />
    /// <exception cref="MusicLibrarySuiteForwardStatusCodeException">Thrown if an unexpected client error occurs.</exception>
    /// <exception cref="ApiException{ProblemDetails}">Thrown if an unexpected server error occurs.</exception>
    /// <exception cref="ApiException">Thrown if an unexpected server error occurs.</exception>
    public async Task<CatalogEntryModel[]> GetCatalogEntriesAsync(IEnumerable<CatalogEntryId> catalogEntryIds, CancellationToken cancellationToken = default)
    {
        IEnumerable<Guid> catalogEntryIdValues = catalogEntryIds.Select(catalogEntryId => catalogEntryId.Value);
        ICollection<CatalogEntryModel> catalogEntryModels = await ExceptionMapperProxy.InvokeAsync(
            async (cancellationToken) => await m_catalogServiceClient.GetCatalogEntriesAsync(catalogEntryIdValues, cancellationToken),
            cancellationToken);
        return catalogEntryModels.ToArray();
    }

    /// <inheritdoc />
    /// <exception cref="MusicLibrarySuiteForwardStatusCodeException">Thrown if an unexpected client error occurs.</exception>
    /// <exception cref="ApiException{ProblemDetails}">Thrown if an unexpected server error occurs.</exception>
    /// <exception cref="ApiException">Thrown if an unexpected server error occurs.</exception>
    [SuppressMessage("Style", "IDE0200:Remove unnecessary lambda expression", Justification = "To increase readability for async cancellable calls.")]
    public async Task<CatalogEntryModel[]> GetCatalogEntriesAsync(CancellationToken cancellationToken = default)
    {
        ICollection<CatalogEntryModel> catalogEntryModels = await ExceptionMapperProxy.InvokeAsync(
            async (cancellationToken) => await m_catalogServiceClient.GetAllCatalogEntriesAsync(cancellationToken),
            cancellationToken);
        return catalogEntryModels.ToArray();
    }

    /// <inheritdoc />
    /// <exception cref="MusicLibrarySuiteForwardStatusCodeException">Thrown if an unexpected client error occurs.</exception>
    /// <exception cref="ApiException{ProblemDetails}">Thrown if an unexpected server error occurs.</exception>
    /// <exception cref="ApiException">Thrown if an unexpected server error occurs.</exception>
    [SuppressMessage("Style", "IDE0200:Remove unnecessary lambda expression", Justification = "To increase readability for async cancellable calls.")]
    public async Task<int> CountCatalogEntriesAsync(CancellationToken cancellationToken = default)
    {
        int catalogEntriesCount = await ExceptionMapperProxy.InvokeAsync(
            async (cancellationToken) => await m_catalogServiceClient.CountAllCatalogEntriesAsync(cancellationToken),
            cancellationToken);
        return catalogEntriesCount;
    }

    /// <inheritdoc />
    /// <exception cref="MusicLibrarySuiteForwardStatusCodeException">Thrown if an unexpected client error occurs.</exception>
    /// <exception cref="ApiException{ProblemDetails}">Thrown if an unexpected server error occurs.</exception>
    /// <exception cref="ApiException">Thrown if an unexpected server error occurs.</exception>
    public async Task<CatalogEntryModel> AddCatalogEntryAsync(CatalogEntryModel catalogEntryModel, CancellationToken cancellationToken = default)
    {
        CatalogEntryModel addedCatalogEntryModel = await ExceptionMapperProxy.InvokeAsync(
            async (cancellationToken) => await m_catalogServiceClient.AddCatalogEntryAsync(catalogEntryModel, cancellationToken),
            cancellationToken);
        return addedCatalogEntryModel;
    }

    /// <inheritdoc />
    /// <exception cref="MusicLibrarySuiteForwardStatusCodeException">Thrown if an unexpected client error occurs.</exception>
    /// <exception cref="ApiException{ProblemDetails}">Thrown if an unexpected server error occurs.</exception>
    /// <exception cref="ApiException">Thrown if an unexpected server error occurs.</exception>
    public async Task UpdateCatalogEntryAsync(CatalogEntryModel catalogEntryModel, CancellationToken cancellationToken = default)
    {
        await ExceptionMapperProxy.InvokeAsync(
            async (cancellationToken) => await m_catalogServiceClient.UpdateCatalogEntryAsync(catalogEntryModel, cancellationToken),
            cancellationToken);
    }

    /// <inheritdoc />
    /// <exception cref="MusicLibrarySuiteForwardStatusCodeException">Thrown if an unexpected client error occurs.</exception>
    /// <exception cref="ApiException{ProblemDetails}">Thrown if an unexpected server error occurs.</exception>
    /// <exception cref="ApiException">Thrown if an unexpected server error occurs.</exception>
    public async Task RemoveCatalogEntryAsync(CatalogEntryId catalogEntryId, CancellationToken cancellationToken = default)
    {
        Guid catalogEntryIdValue = catalogEntryId.Value;
        await ExceptionMapperProxy.InvokeAsync(
            async (cancellationToken) => await m_catalogServiceClient.RemoveCatalogEntryAsync(catalogEntryIdValue, cancellationToken),
            cancellationToken);
    }
}
