using System.Diagnostics.CodeAnalysis;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using MusicLibrarySuite.Infrastructure.Exceptions;
using MusicLibrarySuite.Infrastructure.Exceptions.Services.Abstractions;
using MusicLibrarySuite.MicroserviceClients.Exceptions;
using MusicLibrarySuite.MicroserviceClients.Extensions;

namespace MusicLibrarySuite.Application.Core.Services.Base;

/// <summary>
/// Represents the base class for all proxy services.
/// </summary>
public abstract class ProxyServiceBase
{
    /// <summary>
    /// Gets the exception mapper proxy that performs exception-prone calls and conditionally maps thrown exceptions.
    /// </summary>
    protected IExceptionMapperProxy ExceptionMapperProxy { get; }

    /// <summary>
    /// Initializes a new instance of the <see cref="ProxyServiceBase" /> type using the specified services.
    /// </summary>
    /// <param name="exceptionMapperProxy">The exception mapper proxy that performs exception-prone calls and conditionally maps thrown exceptions.</param>
    protected ProxyServiceBase(IExceptionMapperProxy exceptionMapperProxy)
    {
        ExceptionMapperProxy = exceptionMapperProxy;

        ExceptionMapperProxy.Configure(ConfigureExceptionMapper);
    }

    private static void ConfigureExceptionMapper(IExceptionMapperBuilder exceptionMapperBuilder)
    {
        // Map client-error responses with details to exceptions.
        exceptionMapperBuilder.AddApiExceptionRule(
            statusCode: StatusCodes.Status404NotFound,
            remoteExceptionType: typeof(EntityNotFoundException),
            predicate: (apiException) => true,
            factoryMethod: (apiException) => !string.IsNullOrEmpty(apiException.Result.Detail)
                ? new EntityNotFoundException(apiException.Result.Detail, apiException)
                : new EntityNotFoundException());

        exceptionMapperBuilder.AddApiExceptionRule(
            statusCode: StatusCodes.Status409Conflict,
            remoteExceptionType: typeof(EntityConflictException),
            predicate: (apiException) => true,
            factoryMethod: (apiException) => !string.IsNullOrEmpty(apiException.Result.Detail)
                ? new EntityConflictException(apiException.Result.Detail, apiException)
                : new EntityConflictException());

        exceptionMapperBuilder.AddApiExceptionRule(
            statusCode: StatusCodes.Status409Conflict,
            remoteExceptionType: typeof(EntityConstraintViolationException),
            predicate: (apiException) => true,
            factoryMethod: (apiException) => !string.IsNullOrEmpty(apiException.Result.Detail)
                ? new EntityConstraintViolationException(apiException.Result.Detail, apiException)
                : new EntityConstraintViolationException());

        // Forward client-error responses with details
        exceptionMapperBuilder.AddApiExceptionRule(
            predicate: (ApiException<ProblemDetails> apiException) => IsClientErrorStatusCode(apiException.StatusCode),
            factoryMethod: (ApiException<ProblemDetails> apiException) =>
                new MusicLibrarySuiteForwardStatusCodeException(apiException.StatusCode, GetStatusMessage(apiException), apiException.Message, apiException));

        // Forward client-error responses with no details.
        exceptionMapperBuilder.AddApiExceptionRule(
            predicate: (ApiException apiException) => IsClientErrorStatusCode(apiException.StatusCode),
            factoryMethod: (ApiException apiException) =>
                new MusicLibrarySuiteForwardStatusCodeException(apiException.StatusCode, GetStatusMessage(apiException), apiException.Message, apiException));
    }

    private static bool IsClientErrorStatusCode(int statusCode)
    {
        return statusCode >= 400 && statusCode <= 499;
    }

    [SuppressMessage("Style", "IDE0002:Simplify member access", Justification = "To use the same exception class as above.")]
    private static string GetStatusMessage(ApiException apiException)
    {
        return apiException.StatusMessage ?? MusicLibrarySuiteForwardStatusCodeException.UnspecifiedErrorStatusMessage;
    }
}
